package com.EJabezCentumCoKe.MartinOmburaNotificati8Q5.graph;

import org.jgrapht.EdgeFactory;

/**
 * Created by martinomburajr on 9/11/2017.
 */

public class Path implements EdgeFactory<Store,Path>{

    public Store sourceStore;
    public Store targetStore;
    public Path (Store sourceStore, Store targetStore) {
        this.sourceStore = sourceStore;
        this.targetStore = targetStore;
    }

    @Override
    public Path createEdge(Store sourceVertex, Store targetVertex) {
        return null;
    }

    public String toString() {
        return "Path from " + sourceStore.toString() + "to" + targetStore.toString();
    }
}
