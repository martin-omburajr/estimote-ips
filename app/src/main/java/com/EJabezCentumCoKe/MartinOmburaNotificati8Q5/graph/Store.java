package com.EJabezCentumCoKe.MartinOmburaNotificati8Q5.graph;

import org.jgrapht.VertexFactory;

/**
 * Created by martinomburajr on 9/11/2017.
 */

public class Store implements VertexFactory<Store> {
    public String name;

    public Store(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    @Override
    public Store createVertex() {
        return this;
    }
}